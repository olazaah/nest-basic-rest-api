import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { CreateItemDto } from './dto/create-item.dto';
import { ItemsService } from './items.service';
import { Item } from './interfaces/item.interface';

@Controller('items')
export class ItemsController {
    constructor(private readonly ItemsService: ItemsService) {}
    @Get()
    async findAll(): Promise<Item[]> {
        return this.ItemsService.findAll()
    }

    @Get(':id')
    async findOne(@Param('id') id): Promise<Item> {
        return this.ItemsService.findOne(id)
    }

    @Post()
    create(@Body() createItemDto: CreateItemDto): Promise<Item> {
        return this.ItemsService.createItem(createItemDto)
    }

    @Delete(':id')
    delete(@Param('id') id: string): Promise<void> {
        return this.ItemsService.deleteItem(id)
    }

    @Put(':id')
    update(@Body() updateItemDto: CreateItemDto, @Param('id') id): Promise<Item> {
        return this.ItemsService.update(id, updateItemDto)
    }
}
