import { Injectable, NotFoundException } from '@nestjs/common';
import { Item } from './interfaces/item.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class ItemsService {
    constructor(@InjectModel('Item') private readonly itemModel: Model<Item>) {}

    async findAll(): Promise<Item[]> {
        return await this.itemModel.find()
    }

    async findOne(id: string): Promise<Item> {
        return await this.itemModel.findOne({ _id: id})
    }

    async createItem(item: Item): Promise<Item> {
        const newItem = new this.itemModel(item)
        return await newItem.save()
    }

    async deleteItem(id: string): Promise<void> {
        try {
            await this.itemModel.deleteOne({ _id: id })
        } catch (error) {
            throw new NotFoundException(`Item with id -> ${id} not found`)
        }
 
    }

    async update(id: string, item: Item): Promise<Item> {
        return await this.itemModel.findByIdAndUpdate(id, item, { new: true })
    }
}
